# AI Center Manifold Approximation



## Getting started

This is a comparison of 4 types of ML algorithms to solve for a Center Manifold approximation of equations of motion for inflection point quintessence models of dark energy. The types are Markov Chain Monte Carlo (MCMC), Particle Swarm Optimization (PSO), Neural Network (NN) based on an image (plot) of the log of the gradient magnitude, and a NN based on slow manifold (subspace of the center manifold) data.

This is a Python Jupyter Notebook.
